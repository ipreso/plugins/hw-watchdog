<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
require_once 'Actions/Plugin/Skeleton.php';
require_once 'Actions/Plugin/WatchdogTable.php';

class Actions_Plugin_Watchdog extends Actions_Plugin_Skeleton
{
    public function __construct ()
    {
        $this->_name        = 'Watchdog';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Watchdog'));
    }

    public function getContextProperties ($context)
    {
        list ($type, $id) = explode ('-', $context);
        switch ($type)
        {
            case 'gr':
                return (NULL);
                break;
            case 'box':
                $properties = $this->getBoxProperties ($id);
                break;
            default:
                return (NULL);
        }

        return ($properties->getHTMLValues ());
    }

    protected function getBoxProperties ($id)
    {
        $table = new Actions_Plugin_WatchdogTable ();
        $result = $table->get ("$id");
        if ($result == false)
        {
            // Create default values
            $table->set ("$id", "enabled");
            $result = $table->get ("$id");
        }

        $watchdog_status = $result ['status'];

        $htmlProperties = array ();
        $htmlProperties [] = $this->createSelectProperty (
                                'watchdog_status',
                                $this->getTranslation ('Watchdog'),
                                array ('enabled' => $this->getTranslation ('Enabled'),
                                       'disabled' => $this->getTranslation ('Disabled')),
                                $watchdog_status);

        $properties = new Actions_Property ();

        $properties->setItem          ($this->_name);
        $properties->setContext       ("box-".$id);
        $properties->setProperties    ($htmlProperties);

        return ($properties);
    }

    public function setParamBox ($hash, $param, $value)
    {
        return ($this->setParam ($hash, $param, $value));
    }

    protected function setParam ($id, $param, $value)
    {
        $table = new Actions_Plugin_WatchdogTable ();

        switch ($param)
        {
            case "watchdog_status":
                $table->setStatus ($id, $value);
                break;
        }
    }

    public function getPlayerCommand ($hash)
    {
        $toSend = array ();

        $table = new Actions_Plugin_WatchdogTable ();

        $result = $table->get ($hash);
        if ($result && $result ['updated'] == 1)
        {
            // Send new watchdog status
            $toSend = array ('cmd'      => "PLUGIN",
                             'params'   => "watchdog.cmd ".$result ['status']);
            $table->setRead ($hash);
            return ($toSend);
        }

        return (false);
    }
}

