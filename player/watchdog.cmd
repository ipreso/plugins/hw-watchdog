#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====

#
# Variables
#
CONF=/var/lib/iplayer/cmd/audio.conf

###############################################################################
# Log everything as player does
function iInfo
{
    logger -p local4.info -t Watchdog -- "$@"
}
function iError
{
    logger -p local4.err -t Watchdog -- "$@"
}

function usage
{
    echo "Usage:"
    echo "$0 <enabled|disabled>"
    echo ""
}

function failure
{
    iError "$1"
    exit 1
}

function getMyConfFile ()
{
    echo "$CONF"
}

function parseConf ()
{
    CONFFILE=$( getMyConfFile )
    
    if [ ! -f "$CONFFILE" ];
    then
        STATUS="enabled"
        saveConf
    fi

    STATUS=`grep -E '^STATUS=' $CONFFILE | head -n1 | sed -r 's/^STATUS=//g'`
}

function saveConf ()
{
    CONFFILE=$(getMyConfFile)

    echo "STATUS=${STATUS}
" > $CONFFILE

    if [ "$EUID" = "0" ];
    then
        chown player:player $CONFFILE
    fi
}

function enableWatchdog
{
    iInfo "Check if Hardware Watchdog is available"
    iInfo "Configure Watchdog"
    iInfo "Enable Watchdog"
    return 0
}

function configureWatchdog ()
{
    case $1 in
        enabled)
            iInfo "Enabling Watchdog..."
            enableWatchdog || failure "Unable to enable Watchdog"
            ;;
        disabled)
            iInfo "Disabling Watchdog..."
            disableWatchdog || failure "Unable to disable Watchdog"
            ;;
        *)
            iError "Don't know parameter '${1}'"
            ;;
    esac
}

###############################################################################

# This script use player permissions, even if launched as root.
if [ "${EUID}" = "0" ];
then
    iInfo "Running $( basename $0 ) as user 'player'..."
    if [[ x = x$@ ]] ; then
        sudo -u player $0
    else
        sudo -u player $0 "$@"
    fi
    exit $?
fi

parseConf

# What should we do ?
if [ "$#" -ne "1" ] ; then
    usage $( basename $0 )
    exit 1
fi

# Configure audio's output
STATUS="$1"
configureWatchdog "${STATUS}"
saveConf

exit 0

# vi:syntax=sh
